# -*- coding:utf-8 -*-

import re
import os
import time
import multiprocessing
import logging
import logging.handlers

import requests

from http_client import super_get

DEFAULT_URL = 'http://www.avmoo.net/cn/actresses/currentPage/'
PAGE_TAG = '/currentPage/'
DEFAULT_IMAGE_PATH = '/home/yueguang/AvStars'

MANAGER_URL = 'http://localhost:8888/'
GET_INDEX_URL = 'get_task_id'
CHECK_URL = 'url_check'
STATUS_REPORT_URL = 'status_report'

TIMEOUT = (100, 100)

LOG_FILE = 'spider'
LOG_FILE_SIZE = 10 * 1024 * 1024


class Spider:
    def __init__(self, spider_id):
        super().__init__()
        self.spider_id = spider_id

        self.logger = logging.getLogger('spider' + str(spider_id))
        fmt = '%(asctime)s - %(filename)s:%(lineno)s - %(name)s - %(message)s'
        formatter = logging.Formatter(fmt)
        handler = logging.handlers.RotatingFileHandler(LOG_FILE + str(spider_id) + '.log',
                                                       maxBytes=LOG_FILE_SIZE, backupCount=5)  # 实例化handler
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        self.logger.setLevel(logging.DEBUG)

    def get_list_page_index(self):
        while True:
            try:
                index = requests.get(MANAGER_URL + GET_INDEX_URL, timeout=TIMEOUT).text
                if int(index) != -1:
                    url = DEFAULT_URL + str(index)
                    self.get_star_list_page(url)
                else:
                    self.logger.info('爬虫:' + str(self.spider_id) + '任务完成')
                    break
            except Exception as e:
                self.log('等待任务下发:', e)
                time.sleep(1)

    def get_star_list_page(self, url):
        self.logger.info('获取AV列表URL:' + url)
        try:
            response = super_get(url)
            if response.status_code != 200:
                self.log('请求AV女优列表失败,URL:' + url)
                return
            pattern = re.compile(r'<a class="avatar-box text-center" href=".+">')
            all_star_url = pattern.findall(response.text)
            pattern = re.compile(r'<span>.+</span>')
            all_star_name = pattern.findall(response.text)
            for j in range(0, len(all_star_url)):
                star_name = all_star_name[j][6: -7]
                star_url = all_star_url[j][40: -2] + PAGE_TAG
                self.handle_av_star(star_name, star_url, 1)
        except Exception as e:
            self.log('女优列表页面请求失败:', e)

    def handle_av_star(self, star_name, star_url, page):
        temp_url = star_url + str(page)
        try:
            response = super_get(temp_url)
            if response.status_code != 200:
                self.log('请求AV女优页面失败,URL:' + temp_url)
                return
            self.log('开始处理AV女优 ' + star_name + ' URL:' + temp_url)
            self.handle_star_info(star_name, response.text)
            pattern = re.compile(r'<a class="movie-box" href=".+">')
            all_image_info = pattern.findall(response.text)
            pattern = re.compile(r'<date>[^-]+?-?[^-]+?</date>')
            all_code_info = pattern.findall(response.text)
            success_count = 0
            for i in range(0, len(all_image_info)):
                is_need_handle = True
                new_url = all_image_info[i][27:-2]
                av_url = ''
                if new_url.startswith('http:'):
                    av_url += new_url
                elif new_url.startswith('//'):
                    av_url += 'http:' + new_url
                else:
                    is_need_handle = False
                    self.log('发现不合法AV URL:' + new_url)

                code = all_code_info[i][6: -7]
                path = os.path.join(DEFAULT_IMAGE_PATH, star_name)
                av_image = os.path.join(path, code + '.jpg')
                if os.path.exists(av_image):
                    is_need_handle = False
                    self.log('AV URL处理以前已完成:' + av_url)
                    success_count += 1

                if is_need_handle:
                    self.log('开始处理AV,URL:' + av_url)
                    if self.handle_av_url(star_name, av_url):
                        success_count += 1

            if success_count == len(all_image_info):
                self.log('当前页全部AV处理完成:' + temp_url)
            pattern = re.compile(r'>下一页</a>')
            has_next = pattern.search(response.text)
            if has_next:
                self.log('处理下一页')
                self.handle_av_star(star_name, star_url, page + 1)
            else:
                self.log('当前AV女优 ' + star_name + ' 已处理到最后一页')
        except Exception as e:
            self.log('女优页面请求失败:', e)

    def handle_star_info(self, star_name, response):
        self.log('开始处理女优 ' + star_name + ' 信息')
        path = os.path.join(DEFAULT_IMAGE_PATH, star_name)
        if not os.path.exists(path):
            os.makedirs(path)
        if not os.path.exists(path + '/' + star_name + '.txt'):
            pattern = re.compile(r'<div class="photo-info">[\s\S]+?更多无码影片</a></p>')
            all_info = pattern.search(response)
            if all_info:
                with open(path + '/' + star_name + '.txt', 'w') as info_file:
                    pattern = re.compile(r'<p>.+?: .+?</p>')
                    all_info = pattern.findall(all_info.group())
                    for info_str in all_info:
                        info = info_str[3: -4]
                        info_file.write(info + '\n')
                        self.logger.info(info)
        else:
            self.log('女优 ' + star_name + ' 信息已存在')
        self.handle_star_photo(star_name, response)
        self.log('女优 ' + star_name + ' 信息处理已完成')

    def handle_star_photo(self, star_name, response):
        path = os.path.join(DEFAULT_IMAGE_PATH, star_name)
        if not os.path.exists(path):
            os.makedirs(path)
        pattern = re.compile(r'<div class="avatar-box">[\s\S]+?<div class="photo-frame">[\s\S]+?</div>')
        photo_info = pattern.search(response)
        if photo_info:
            pattern = re.compile(r'src=.+ title')
            photo_temp = pattern.search(photo_info.group())
            if photo_temp:
                photo = photo_temp.group()[5: -7]
                photo_url = None
                if photo.startswith('http://'):
                    photo_url = photo
                elif photo.startswith('//'):
                    photo_url = 'http:' + photo
                else:
                    self.log('请求AV女优头像失败,地址非法,URL:' + photo)
                if photo_url is not None:
                    info = photo_url.split('.')
                    image = os.path.join(path, star_name + '.' + info[len(info) - 1])
                    if not os.path.exists(image):
                        try:
                            response = super_get(photo_url)
                            if response.status_code != 200:
                                self.log('请求AV女优头像失败,URL:' + photo_url)
                                return
                            with open(image, 'wb') as f:
                                f.write(response.content)
                                self.log(star_name + '头像保存完成')
                        except Exception as e:
                            self.log('女优 ' + star_name + ' 头像请求失败:', e)
                    else:
                        self.log('女优 ' + star_name + ' 头像已存在')

    def handle_av_url(self, name, url):
        result = False
        try:
            response = super_get(url)
            if response.status_code != 200:
                self.log('请求AV信息页面失败,URL:' + url)
            else:
                pattern = re.compile(r'<h3>.+</h3>')
                av_name_info = pattern.search(response.text)
                av_name_info = av_name_info.group()[4: -5].split(' ')
                code = av_name_info[0]
                pattern = re.compile(r'<a class="bigImage" href=".+?">')
                av_image_info = pattern.search(response.text)
                if av_image_info:
                    url_end = av_image_info.group()[26: -2]
                    av_image_url = None
                    if url_end.startswith('http:'):
                        av_image_url = url_end
                    elif url_end.startswith('//'):
                        av_image_url = 'http:' + url_end
                    else:
                        self.log('AV封面URL非法,错误页:' + url)
                    if av_image_url is not None:
                        self.log('开始处理AV封面URL:' + av_image_url)
                        if self.save_image(name, av_image_url, code):
                            self.handle_av_info(name, response.text)
                            result = True
                else:
                    self.log('AV封面URL未找到,错误页:' + url)
        except Exception as e:
            self.log('AV信息页面请求失败:', e)
        return result

    def handle_av_info(self, star_name, response):
        self.log('开始处理AV信息')
        path = os.path.join(DEFAULT_IMAGE_PATH, star_name)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(path + '/' + star_name + '.txt', 'a') as info_file:
            info_file.write('***********************************\n')
            pattern = re.compile(r'<h3>.+</h3>')
            av_name_info = pattern.search(response)
            av_name_info = av_name_info.group()[4: -5].split(' ')
            code = av_name_info[0]
            av_name = av_name_info[1]
            info_file.write('番号:' + code + '\n')
            self.logger.info('番号:' + code)
            info_file.write('标题:' + av_name + '\n')
            self.logger.info('标题:' + av_name)

            pattern = re.compile(r'发行时间:</span> .+</p>')
            send_time_info = pattern.search(response)
            if send_time_info:
                send_time = send_time_info.group()[13: -4]
                info_file.write('发行时间:' + send_time + '\n')
                self.logger.info('发行时间:' + send_time)

            pattern = re.compile(r'长度:</span> .+</p>')
            len_info = pattern.search(response)
            if len_info:
                length = len_info.group()[11: -4]
                info_file.write('长度:' + length + '\n')
                self.logger.info('长度:' + length)

            pattern = re.compile(r'制作商: </p>[\s\S]+?</p>')
            make_info = pattern.search(response)
            if make_info:
                make = make_info.group()[0: -8].split('>')[-1]
                info_file.write('制作商:' + make + '\n')
                self.logger.info('制作商:' + make)

            pattern = re.compile(r'发行商: </p>[\s\S]+?</p>')
            publisher_info = pattern.search(response)
            if publisher_info:
                publisher = publisher_info.group()[0: -8].split('>')[-1]
                info_file.write('发行商:' + publisher + '\n')
                self.logger.info('发行商:' + publisher)

            pattern = re.compile(r'类别:</p>[\s\S]+?<p>[\s\S]+?</p>')
            tag_info = pattern.search(response)
            if tag_info:
                pattern = re.compile(r'>.+?</a>')
                all_tag_info = pattern.findall(tag_info.group())
                tag_str = '类别:'
                for tag in all_tag_info:
                    tag_str = tag_str + tag.split('>')[-2].split('<')[0] + ' '
                self.logger.info(tag_str)
                tag_str += '\n'
                info_file.write(tag_str)

            pattern = re.compile(r'<a class="avatar-box" href=[\s\S]+?</a>')
            star_info = pattern.findall(response)
            star_pattern = re.compile(r'<span>.+</span>')
            all_star = '演员:'
            for star in star_info:
                star = star_pattern.search(star)
                all_star = all_star + star.group()[6: -7] + ' '
            self.logger.info(all_star)
            all_star += '\n'
            info_file.write(all_star)
            self.log('AV信息处理已完成')
            return code

    def save_image(self, name, url, code):
        result = False
        try:
            response = super_get(url)
            if response.status_code != 200:
                self.log('请求AV封面失败,URL:' + url)
            else:
                path = os.path.join(DEFAULT_IMAGE_PATH, name)
                if not os.path.exists(path):
                    os.makedirs(path)
                info = url.split('.')
                image = os.path.join(path, code + '.' + info[len(info) - 1])
                with open(image, 'wb') as f:
                    f.write(response.content)
                result = True
                self.log('AV封面处理已完成')
        except Exception as e:
            self.log('AV封面请求失败', e)
        return result

    def log(self, msg, e=None):
        if e is None:
            print(msg)
        else:
            print(msg, e)
        self.logger.info(msg)


def start(num):
    if not os.path.exists(DEFAULT_IMAGE_PATH):
        os.makedirs(DEFAULT_IMAGE_PATH)
    for i in range(num):
        spider_worker = Spider(i)
        p = multiprocessing.Process(target=spider_worker.get_list_page_index)
        p.start()


if __name__ == '__main__':
    # spider = Spider(0)
    # spider.get_list_page_index()
    spider = Spider(0)
    spider.handle_av_star('ゆう', 'http://www.avmoo.net/cn/star/9gx' + PAGE_TAG, 1)
    # get_star_list_page(DEFAULT_URL + '1')
