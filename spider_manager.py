# -*- coding:utf-8 -*-

import time
import logging
import logging.handlers
import base64

import tornado.ioloop
import tornado.web

import spider

PROCESS_MAX_NUM = 3

MAX_PAGE_INDEX = 183
current_index = 183

HISTORY_FILE = 'history.txt'
history = []

LOG_FILE = 'manager.log'
logger = None
LOG_FILE_SIZE = 10 * 1024 * 1024


class TaskHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    # def get(self):
    #     global current_index
    #     current_index += 1
    #     if current_index <= MAX_PAGE_INDEX:
    #         self.write(str(current_index))
    #         logger.info('分配: ' + str(current_index))
    #     else:
    #         self.write(str(-1))
    #         logger.info('所有爬取任务完成!')

    def get(self):
        global current_index
        current_index -= 1
        if current_index >= 1:
            self.write(str(current_index))
            logger.info('分配: ' + str(current_index))
        else:
            self.write(str(-1))
            logger.info('所有爬取任务完成!')


class UrlCheckHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        url = bytes.decode(base64.urlsafe_b64decode(self.get_argument('url')))
        if url in history:
            logger.info('URL存在完成记录:' + url)
            self.write("True")
        else:
            logger.info('URL不存在完成记录:' + url)
            self.write("False")


class StatusReportHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        pass

    def get(self):
        url = bytes.decode(base64.urlsafe_b64decode(self.get_argument('url')))
        status_code = self.get_argument('status')
        if int(status_code) == 200:
            global history
            history.append(url)
            with open(HISTORY_FILE, 'a') as history_file:
                history_info = time.strftime('%Y/%m/%d %X') + '\t' + url + '\n'
                history_file.write(history_info)
            logger.info('Url已完成:' + url)
        else:
            logger.info('Url处理出错:' + url)
        self.write('True')


def make_app():
    return tornado.web.Application([
        (r"/get_task_id", TaskHandler),
        (r"/url_check", UrlCheckHandler),
        (r"/status_report", StatusReportHandler)
    ])


def init_log():
    global logger
    logger = logging.getLogger('manager')

    fmt = '%(asctime)s - %(filename)s:%(lineno)s - %(name)s - %(message)s'
    formatter = logging.Formatter(fmt)
    handler = logging.handlers.RotatingFileHandler(LOG_FILE, maxBytes=LOG_FILE_SIZE, backupCount=5)  # 实例化handler
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # console = logging.StreamHandler(sys.stdout)
    # console.setLevel(logging.INFO)
    # logger.addHandler(console)

    logger.setLevel(logging.DEBUG)


if __name__ == "__main__":
    init_log()

    spider.start(PROCESS_MAX_NUM)

    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
