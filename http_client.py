# -*- coding:utf-8 -*-

import time

import requests

__author__ = 'yueguang'

TIMEOUT = (5, 100)
GET_PROXY_URL = 'http://54.238.171.167:9999/get_proxy'
RETRY_LIMIT = 5
USER_AGENT = {
    'User-agent': 'Mozilla/5.0 (X11; Fedora; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116'}


def super_get(url, time_out=TIMEOUT, payload=None, proxies=None):
    response = None
    retry_count = 0
    while True:
        if retry_count != 0:
            if retry_count < RETRY_LIMIT:
                proxy = requests.get(GET_PROXY_URL)
                a = eval(proxy.text)
                protocol = a['protocol']
                ip = a['ip']
                port = a['port']
                proxies = {protocol: protocol + '://' + ip + ':' + port}
                print('使用代理:' + str(proxies))
            else:
                print('超过重试次数,停止请求')
                break
        retry_count += 1
        try:
            response = requests.get(url, headers=USER_AGENT, timeout=time_out, params=payload, proxies=proxies)
            if response.status_code == 200:
                break
            elif response.status_code != 403:
                print('请求失败URL:' + url + ' 响应码:' + str(response.status_code))
                break
        except ConnectionResetError:
            print('请求:' + url + ' 被重置')
            time.sleep(1)
        except Exception as e:
            print('请求:' + url + ' 发生异常:', e)
            break
    return response


if __name__ == '__main__':
    print(super_get('http://www.avmoo.net/cn/movie/qnt'))
